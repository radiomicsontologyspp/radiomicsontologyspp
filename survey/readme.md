This directory contains the anonymized results of all rounds of the deplhi process conducted by the SPP in order to find a consensus radiomics workflow definition.

The delphi process can be divided into 2 objectives:
1. Workflow Definition Consensus Process  (Round 1, 2 and 4)
2. Challenge Characterization Process (Round 3 and 5)
