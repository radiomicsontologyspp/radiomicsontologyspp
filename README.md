# RadiomicsOntologySPP

This project will provide a space for collaborative editing and publishing of an ontology derived from the results of the radiomics workflow definition consensus process which was conducted within the DFG priority program [Radiomics: Next Generation of Biomedical Imaging (SPP 2177)](https://www.uniklinik-freiburg.de/radiomics.html)

